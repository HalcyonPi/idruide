const path = require('path');

const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const Dotenv = require('dotenv-webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { GenerateSW } = require('workbox-webpack-plugin');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');

const outputDirectory = 'dist';

module.exports = (env, argv) => ({
  node: {
    dns: 'empty',
    fs: 'empty',
    net: 'empty',
  },
  entry: ['@babel/polyfill', './src/client/js/index.js'],
  output: {
    path: path.resolve(__dirname, outputDirectory),
    publicPath: '/',
    filename: 'bundle.js'
  },
  devServer: {
    contentBase: path.resolve(__dirname, outputDirectory),
    historyApiFallback: true,
    port: 3000,
    open: true,
  },
  devtool: env.production ? 'source-map' : 'cheap-module-eval-source-map',
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /node_modules/,
      loader: 'babel-loader',
    }, {
      test: /\.s?css$/,
      use: [
        MiniCssExtractPlugin.loader,
        {
          loader: 'css-loader',
          options: {
            sourceMap: true
          }
        }, {
          loader: 'sass-loader',
          options: {
            sourceMap: true
          }
        }
      ]
    }, {
      test: /\.(png|svg|jpg|gif|webp|eot|ttf|woff)$/,
      use: [{
        loader: 'url-loader',
        options: {
          fallback: 'file-loader',
          limit: 8192,
          name: '[path][name]-[hash:8].[ext]'
        },
      }]
    }]
  },
  plugins: env.production ? [
    new Dotenv({ systemvars: true }),
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: './public/index.html',
      favicon: './public/favicon.ico'
    }),
    new MiniCssExtractPlugin({
      filename: 'styles.css'
    }),
    new GenerateSW({
      swDest: 'sw.js',
      clientsClaim: true,
      skipWaiting: true,

      // Exclude images from the precache
      exclude: [/\.(?:png|jpg|jpeg|svg|webp)$/],

      // Enable offline Google Analytics
      offlineGoogleAnalytics: true,

      // Define runtime caching rules.
      runtimeCaching: [{
        // Match any request ends with .png, .jpg, .jpeg or .svg.
        urlPattern: /\.(?:png|jpg|jpeg|svg|webp)$/,

        // Apply a stale-while-revalidate strategy.
        handler: 'StaleWhileRevalidate',

        options: {
          // Use a custom cache name.
          cacheName: 'image-cache',

          // Only cache 50 images for 7d.
          expiration: {
            maxEntries: 50,
            maxAgeSeconds: 7 * 24 * 60 * 60,
          },
        },
      }, {
        // Match any request ends with .css.
        urlPattern: /\.(?:css)$/,

        // Apply a cache-first strategy.
        handler: 'CacheFirst',

        options: {
          // Use a custom cache name.
          cacheName: 'styles-cache',

          // Only cache for 24h.
          expiration: {
            maxAgeSeconds: 24 * 60 * 60,
          },
        },
      }],
    }),
  ]
    : [
      new Dotenv({ systemvars: true }),
      new CleanWebpackPlugin(),
      new HtmlWebpackPlugin({
        filename: 'index.html',
        template: './public/index.html',
        favicon: './public/favicon.ico'
      }),
      new MiniCssExtractPlugin({
        filename: 'styles.css'
      }),
      new BundleAnalyzerPlugin(),
    ],
});
