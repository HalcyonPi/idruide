const listArray = [
  {
    _id: '1',
    name: '',
    category: '',
    checked: false,
    created_at: new Date().toISOString(),
  }, {
    _id: '2',
    name: 'Milk',
    category: 'Dairy',
    checked: false,
    created_at: new Date().toISOString()
  }, {
    _id: '3',
    name: 'Tomatoes',
    category: 'Produce',
    checked: true,
    created_at: new Date().toISOString(),
  },
];

export default listArray;
