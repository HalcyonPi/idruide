import { addExpense, delExpense, editExpense } from '../../client/js/actions/expenses';

test('should setup delExpense action object', () => {
  const action = delExpense({ id: '123abc' });

  expect(action).toEqual({
    type: 'DELETE_EXPENSE',
    id: '123abc'
  });
});

test('should setup editExpense action object with provided values', () => {
  const action = editExpense('123abc', { note: 'New note value' });

  expect(action).toEqual({
    type: 'EDIT_EXPENSE',
    id: '123abc',
    update: { note: 'New note value' },
  });
});

test('should setup addExpense action object with default values', () => {
  const expenseData = {};
  const action = addExpense(expenseData);

  expect(action).toEqual({
    type: 'ADD_EXPENSE',
    expense: {
      id: expect.any(String),
      description: '',
      amount: 0,
      note: '',
      createdAt: 0,
    }
  });
});

test('should setup addExpense action object with provided values', () => {
  const expenseData = {
    description: 'New description',
    amount: 1000,
    note: 'New note',
    createdAt: 1234567890
  };
  const action = addExpense(expenseData);

  expect(action).toEqual({
    type: 'ADD_EXPENSE',
    expense: {
      id: expect.any(String),
      ...expenseData,
    }
  });
});
