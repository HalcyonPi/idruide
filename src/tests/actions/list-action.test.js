import { addListItem, editListItem, removeListItem } from '../../client/js/actions/list-action';
import listArray from '../fixtures/list-fixture';

test('should setup addListItem action object given user input', () => {
  expect(addListItem(listArray[1])).toEqual({
    type: 'ADD_LIST_ITEM',
    listItem: listArray[1]
  });
});

test('should setup editListItem action object given user input', () => {
  expect(editListItem(listArray[1])).toEqual({
    type: 'EDIT_LIST_ITEM',
    listItem: listArray[1]
  });
});

test('should setup removeListItem action object given user input', () => {
  expect(removeListItem(listArray[1])).toEqual({
    type: 'REMOVE_LIST_ITEM',
    listItem: listArray[1]
  });
});
