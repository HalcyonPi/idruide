import handleCapitalize from '../../client/js/_utils/handleCapitalize';

test('should capitalize the first character of a string', () => {
  const strings = [
    'Word',
    'Word word',
    'word',
    'word word',
    '@word',
  ];

  expect(strings.map(el => handleCapitalize(el))).toEqual([
    'Word',
    'Word word',
    'Word',
    'Word word',
    '@word',
  ]);
});
