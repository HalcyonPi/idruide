/*
STATIC ROUTES
*/
module.exports = (app, publicPath, staticPath) => {
  /*
  SITEMAP
  */
  app.get('/sitemap', (request, response) => {
    response.status(200).sendFile(`${staticPath}/sitemap.xml`, {
      headers: {
        'Content-Type': 'application/xml',
      }
    }, (error) => {
      if (error) {
        response.status(500).send(error);
      }
    });
  });

  /*
  ROBOTS
  */
  app.get('/robots', (request, response) => {
    response.status(200).sendFile(`${staticPath}/robots.txt`, {
      headers: {
        'Content-Type': 'text/plain;charset=UTF-8',
      }
    }, (error) => {
      if (error) {
        response.status(500).send(error);
      }
    });
  });

  /*
  PWA ICONS
  */
  app.get('/img/apple-touch-icon', (request, response) => {
    response.status(200).sendFile(`${staticPath}/apple-touch-icon.png`, {
      headers: {
        'Content-Type': 'image/png',
      }
    }, (error) => {
      if (error) {
        response.status(500).send(error);
      }
    });
  });

  app.get('/img/icon192', (request, response) => {
    response.status(200).sendFile(`${staticPath}/app_icon_192.png`, {
      headers: {
        'Content-Type': 'image/png',
      }
    }, (error) => {
      if (error) {
        response.status(500).send(error);
      }
    });
  });

  app.get('/img/icon512', (request, response) => {
    response.status(200).sendFile(`${staticPath}/app_icon_512.png`, {
      headers: {
        'Content-Type': 'image/png',
      }
    }, (error) => {
      if (error) {
        response.status(500).send(error);
      }
    });
  });

  /*
  CATCH-ALL
  */
  app.get('/*', (request, response) => {
    response.sendFile(`${publicPath}/index.html`, (error) => {
      if (error) {
        response.status(500).send(error);
      }
    });
  });
};
