export const getList = () => ({ type: 'GET_LIST' });

export const addListItem = listItem => ({
  type: 'ADD_LIST_ITEM',
  listItem,
});

export const editListItem = listItem => ({
  type: 'EDIT_LIST_ITEM',
  listItem,
});

export const removeListItem = listItem => ({
  type: 'REMOVE_LIST_ITEM',
  listItem,
});
