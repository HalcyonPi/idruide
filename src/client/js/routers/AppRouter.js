import React, {
  Fragment,
  lazy,
  Suspense,
  useEffect,
  useState
} from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import uuid from 'uuid/v1';
import { gaInit, GAListener } from '../analytics/ga';

import Header from '../components/header/Header';
import LoadingSpinner from '../components/misc/LoadingSpinner';

const AppContainer = lazy(() => import('../components/app/AppContainer'));

// Inits react-ga (Google Analytics)
gaInit();

const AppRouter = () => {
  const [isSessionToken, setIsSessionToken] = useState(false);
  const [metaTitle, setMetaTitle] = useState(undefined);

  const handleSessionToken = () => {
    const hasToken = sessionStorage.getItem('uuid');

    if (!hasToken) {
      sessionStorage.setItem('uuid', uuid());
      setIsSessionToken(true);
    } else if (hasToken && !isSessionToken) {
      setIsSessionToken(true);
    }
  };

  useEffect(() => {
    if (!isSessionToken) handleSessionToken();
  }, [isSessionToken]);

  const getMetaTitle = (title) => {
    setMetaTitle(title);
  };

  return (
    <BrowserRouter>

      <Fragment>
        <Header />
        <Suspense fallback={<LoadingSpinner />}>
          <GAListener pageTitle={metaTitle}>
            <Switch>
              <Route
                exact
                path="/"
                render={props => <AppContainer {...props} getMetaTitle={getMetaTitle} />}
              />

            </Switch>
          </GAListener>
        </Suspense>
      </Fragment>

    </BrowserRouter>
  );
};

export default AppRouter;
