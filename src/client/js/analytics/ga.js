import React, { useEffect } from 'react';
import ReactGA from 'react-ga';
import { arrayOf, object, string } from 'prop-types';

const canDebug = false; // process.env.NODE_ENV !== 'production';

export const gaInit = () => {
  ReactGA.initialize('UA-113797323-1', { debug: canDebug });
};

export const GAListener = (props) => {
  const sendPageView = (location) => {
    ReactGA.set({ location: location.href, page: location.pathname, title: props.pageTitle });
    ReactGA.pageview(location.pathname);
  };

  useEffect(() => {
    sendPageView(window.location);
  }, [props.pageTitle]);

  return (
    props.children
  );
};

/*
PropTypes
*/
GAListener.propTypes = {
  children: arrayOf(object).isRequired,
  location: object,
  pageTitle: string,
};

GAListener.defaultProps = {
  location: {},
  pageTitle: '',
};
