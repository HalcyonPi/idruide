/*
FONTAWESOME LIB IMPORTS
*/
import { config, library } from '@fortawesome/fontawesome-svg-core';
import { faCheck } from '@fortawesome/free-solid-svg-icons/faCheck';
import { faPen } from '@fortawesome/free-solid-svg-icons/faPen';
import { faPlus } from '@fortawesome/free-solid-svg-icons/faPlus';
import { faShoppingBasket } from '@fortawesome/free-solid-svg-icons/faShoppingBasket';
import { faTimes } from '@fortawesome/free-solid-svg-icons/faTimes';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons/faTrashAlt';

/*
FONTAWESOME LIB
*/
export default () => {
  library.add(
    faCheck,
    faPen,
    faPlus,
    faShoppingBasket,
    faTimes,
    faTrashAlt,
  );
};
