import React from 'react';
// import { func, number } from 'prop-types';

import HeaderBrand from './HeaderBrand';

const Header = () => (
  <header>
    <nav id="navBarMain" className="navbar navbar-expand-lg">
      <HeaderBrand />
    </nav>
  </header>
);

export default Header;

/*
PropTypes
*/
Header.propTypes = {
};

Header.defaultProps = {
};
