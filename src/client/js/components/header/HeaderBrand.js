import React from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const HeaderBrand = () => (
  <div className="expand navbar-expand">
    <Link to="/" className="navbar-brand mt-2 mt-lg-0">
      <FontAwesomeIcon className="navbar-logo" alt="App logo" icon="shopping-basket" />
      <span>MyGrocies</span>
    </Link>
  </div>
);

export default HeaderBrand;
