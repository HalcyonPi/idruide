import {
  arrayOf,
  bool,
  shape,
  string
} from 'prop-types';

export const productType = shape({
  created_at: string.isRequired,
  updated_at: string,
  _id: string.isRequired,
  display: bool.isRequired,
  id_activity: string.isRequired,
  id_event: string.isRequired,
  id_grandstand: string.isRequired,
  name: string.isRequired,
  url: string.isRequired,
  patterns: arrayOf(string),
});

export const productTypeDefault = {
  created_at: '',
  updated_at: '',
  _id: '',
  display: false,
  id_activity: '',
  id_event: '',
  id_grandstand: '',
  name: '',
  url: '',
  patterns: [],
};
