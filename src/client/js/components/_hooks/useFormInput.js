import { useState } from 'react';

import handleCapitalize from '../../_utils/handleCapitalize';

const useFormInput = (initValue, canCapitalize, isCheckBox) => {
  const [value, setValue] = useState(initValue);

  const handleChange = (e) => {
    const isObject = typeof e === 'object';
    let elementValue;

    if (isObject) {
      elementValue = isCheckBox ? e.target.checked : e.target.value;
    } else {
      elementValue = e;
    }

    if (canCapitalize) {
      elementValue = handleCapitalize(elementValue);
    } else if (!isCheckBox) {
      elementValue = elementValue.replace(/\s+/g, '-').toLowerCase();
    }

    setValue(elementValue);
  };

  const handleClearInput = () => {
    setValue(initValue);
  };

  return {
    value,
    onChange: handleChange,
    onSubmit: handleClearInput,
  };
};

export default useFormInput;
