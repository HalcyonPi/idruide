import React from 'react';
import { Helmet } from 'react-helmet';
import { string } from 'prop-types';

import defaultLogo from '../../../img/logo/logo.svg';

class Meta extends React.PureComponent {
  state = {
    canIndex: false,
  }

  componentDidMount() {
    const isCanonicalDomain = window.location.hostname.includes('mygrocies.herokuapp.com');

    if (isCanonicalDomain) this.setState({ canIndex: true });
  }

  render() {
    return (
      <div className="application">
        <Helmet>
          <meta charSet="utf-8" />
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
          <meta name="theme-color" content="#08aeea" />

          <link rel="canonical" href={this.props.pageUrl} />
          {!this.state.canIndex && (<meta name="robots" content="noindex" />)}

          <meta name="twitter:card" content="summary" />
          <meta name="twitter:site" content="@mygrocies" />
          <meta name="twitter:title" content={this.props.metaTitle} />
          <meta
            name="twitter:description"
            content="Grocery list made smart"
          />
          <meta name="twitter:creator" content="@mygrocies" />
          <meta
            name="twitter:image"
            content={defaultLogo}
          />

          <meta property="og:title" content={this.props.metaTitle} />
          <meta property="og:type" content="website" />
          <meta property="og:url" content={this.props.pageUrl} />
          <meta
            property="og:image"
            content={defaultLogo}
          />
          <meta
            property="og:description"
            content="Grocery list made smart"
          />

          <title lang="en">{this.props.metaTitle}</title>
          <meta
            name="description"
            content="Grocery list made smart"
          />
        </Helmet>
      </div>
    );
  }
}

export default Meta;

/*
PropTypes
*/
Meta.propTypes = {
  metaTitle: string,
  pageUrl: string.isRequired,
};

Meta.defaultProps = {
  metaTitle: 'MyGrocies',
};
