import React, { useContext } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import handleItemDisplay from '../../_utils/handleItemDisplay';
import { editListItem, removeListItem } from '../../actions/list-action';
import ListContext from '../../contexts/list-context';
import useFormInput from '../_hooks/useFormInput';

const ListItemEdit = (listItem) => {
  const { listDispatch } = useContext(ListContext);
  const newName = useFormInput(listItem.name, true);

  const handleEditItem = (e) => {
    e.preventDefault();

    const newItem = { ...listItem };
    newItem.name = newName.value;
    newItem.updated_at = new Date().toISOString();

    listDispatch(editListItem(newItem));
    handleItemDisplay('itemEdit', listItem._id);
  };

  const handleDeleteItem = (e) => {
    e.preventDefault();

    listDispatch(removeListItem(listItem));
  };

  const handleEditItemShow = (e) => {
    e.preventDefault();

    handleItemDisplay('itemEdit', listItem._id);
  };

  return (
    <div id={`itemEdit${listItem._id}`} className="list-item-edit">
      <form>
        <div className="input-group">
          <input type="text" className="form-control" {...newName} />
          <div className="input-group-append">
            <button
              className="btn btn-outline-secondary"
              type="submit"
              onClick={handleEditItem}
            >
              <FontAwesomeIcon icon="check" />
            </button>
          </div>
        </div>
      </form>
      <div className="list-item-action">
        <FontAwesomeIcon className="trash-icon" icon="trash-alt" onClick={handleDeleteItem} />
        <FontAwesomeIcon className="close-icon" icon="times" onClick={handleEditItemShow} />
      </div>
    </div>
  );
};

export default ListItemEdit;

/*
PropTypes
*/
ListItemEdit.propTypes = {
};
