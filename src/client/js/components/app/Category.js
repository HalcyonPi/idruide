import React, { useContext } from 'react';

import ListContext from '../../contexts/list-context';

import CategoryItem from './CategoryItem';


const Category = () => {
  const { categories } = useContext(ListContext);

  const handleList = () => categories.map(el => (
    <CategoryItem
      key={el._id}
      id={el._id}
      name={el.name}
    />
  ));

  return (
    <div id="accordion" className="category">
      {handleList()}
    </div>
  );
};

export default Category;

/*
PropTypes
*/
