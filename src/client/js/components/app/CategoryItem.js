import React, { useContext } from 'react';
import { string } from 'prop-types';

import ListContext from '../../contexts/list-context';

import List from './List';

const CategoryItem = ({ id, name }) => {
  const { list } = useContext(ListContext);

  const categoryList = list.filter(el => el.category === name);
  const categoryListChecked = categoryList.filter(el => el.checked);

  return (
    <div id={`categoryItem${id}`} className="category-item card">
      <div
        className="card-link"
        data-toggle="collapse"
        data-target={`#collapse${id}`}
        aria-expanded="true"
        aria-controls={`collapse${id}`}
      >
        <div className="card-header" id={`heading${id}`}>
          <h2 className="mb-0">
            {name}
          </h2>
          <span className="list-stats">{`${categoryListChecked.length}/${categoryList.length}`}</span>
        </div>
      </div>
      
      <List
        categoryName={name}
        id={id}
      />
    </div>
  );
};

export default CategoryItem;

/*
PropTypes
*/
CategoryItem.propTypes = {
  id: string.isRequired,
  name: string.isRequired,
};
