import React, { useContext, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import handleItemDisplay from '../../_utils/handleItemDisplay';
import { editListItem } from '../../actions/list-action';
import ListContext from '../../contexts/list-context';
import useFormInput from '../_hooks/useFormInput';

import ListItemEdit from './ListItemEdit';

const ListItem = (listItem) => {
  const { listDispatch } = useContext(ListContext);
  const checked = useFormInput(listItem.checked, false, true);

  const handleEditItemShow = (e) => {
    e.preventDefault();

    handleItemDisplay('itemEdit', listItem._id);
  };

  const handleCheckItem = () => {
    const newItem = { ...listItem };
    newItem.checked = checked.value;
    newItem.updated_at = new Date().toISOString();

    listDispatch(editListItem(newItem));
  };

  useEffect(() => {
    if (listItem.checked !== checked.value) handleCheckItem();
  }, [checked.value]);

  return (
    <li>
      <div className="list-item">
        {listItem.name}

        <div className="list-item-action">
          <div className="form-check">
            <input
              className="form-check-input position-static"
              id={`defaultCheck${listItem._id}`}
              type="checkbox"
              aria-label="Check the product"
              defaultChecked={listItem.checked}
              {...checked}
            />
          </div>
          <FontAwesomeIcon className="text-muted" icon="pen" onClick={handleEditItemShow} />
        </div>
      </div>

      <ListItemEdit {...listItem} />
    </li>
  );
};

export default ListItem;

/*
PropTypes
*/
ListItem.propTypes = {
};
