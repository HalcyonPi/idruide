import React, { useContext } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import uuid from 'uuid/v1';

import { addListItem } from '../../actions/list-action';
import ListContext from '../../contexts/list-context';
import useFormInput from '../_hooks/useFormInput';

const AddForm = () => {
  const { categories, listDispatch } = useContext(ListContext);
  const category = useFormInput('', true);
  const name = useFormInput('', true);

  const handleAddItem = (e) => {
    e.preventDefault();

    if (!name.value) {
      alert('Please enter a name');
    } else if (!category.value) {
      alert('Please choose a category');
    } else {
      const listItem = {
        _id: uuid(),
        name: name.value,
        category: category.value,
        checked: false,
        created_at: new Date().toISOString(),
      };

      listDispatch(addListItem(listItem));
      name.onSubmit();
    }
  };

  const handleOption = () => categories.map(el => (
    <option key={el._id} value={el.name}>{el.name}</option>
  ));

  return (
    <form className="add-form" aria-label="Add a product to your grocery list">
      <div className="input-group mb-3">
        <div className="input-group-prepend">
          <select
            id="inputGroupSelect01"
            className="custom-select"
            aria-label="Category selector"
            {...category}
          >
            <option defaultValue>Category</option>
            {handleOption()}
          </select>
        </div>
        <input
          className="form-control"
          type="text"
          placeholder="Milk, soap, pasta..."
          aria-label="Enter a list item"
          {...name}
        />
        <div className="input-group-append">
          <button
            className="btn btn-outline-secondary"
            type="submit"
            aria-label="Add list item"
            onClick={handleAddItem}
          >
            <FontAwesomeIcon icon="plus" />
          </button>
        </div>
      </div>
    </form>
  );
};

export default AddForm;

/*
PropTypes
*/
AddForm.propTypes = {
};
