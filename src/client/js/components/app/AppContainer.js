import React, { useEffect, useReducer, useState } from 'react';
import uuid from 'uuid/v1';
import { func, string } from 'prop-types';

import ListContext from '../../contexts/list-context';
import listReducer from '../../reducers/list-reducer';

import Meta from '../misc/Meta';
import AddForm from './AddForm';
import Category from './Category';

const categoryArr = [
  { name: 'Beverages', _id: uuid() },
  { name: 'Canned Goods', _id: uuid() },
  { name: 'Cleaners', _id: uuid() },
  { name: 'Dairy', _id: uuid() },
  { name: 'Frozen Food', _id: uuid() },
  { name: 'Meat', _id: uuid() },
  { name: 'Personal Care', _id: uuid() },
  { name: 'Produce', _id: uuid() },
];

const AppContainer = props => {
  const [list, listDispatch] = useReducer(listReducer, []);
  const [categories] = useState(categoryArr);

  useEffect(() => {
    window.scrollTo(0, 0);

    const localList = JSON.parse(localStorage.getItem('list'));
    if (Array.isArray(localList)) {
      listDispatch({ type: 'GET_LIST', list: localList });
    }
  }, []);

  useEffect(() => {
    if (list.length > -1) localStorage.setItem('list', JSON.stringify(list));
  }, [list]);

  useEffect(() => {
    props.getMetaTitle(document.title);
  }, [document.title]);

  return (
    <ListContext.Provider value={{ categories, list, listDispatch }}>
      <div className="app container">
        <Meta metaTitle={props.metaTitle} pageUrl={process.env.URL_CANONICAL} />

        <AddForm />
        <Category />
      </div>
    </ListContext.Provider>
  );
};

export default AppContainer;

/*
PropTypes
*/
AppContainer.propTypes = {
  getMetaTitle: func.isRequired,
  metaTitle: string,
};

AppContainer.defaultProps = {
  metaTitle: 'MyGrocies',
};
