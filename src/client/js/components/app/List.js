import React, { useContext } from 'react';
import { string } from 'prop-types';

import ListContext from '../../contexts/list-context';

import ListItem from './ListItem';

const List = ({ categoryName, id }) => {
  const { list } = useContext(ListContext);

  const handleList = () => list.filter(el => el.category === categoryName)
    .sort((a, b) => {
      if (a.name > b.name) {
        return 1;
      } else if (a.name < b.name) {
        return -1;
      } else {
        return 0;
      }
    })
    .map(el => (
      <ListItem key={el._id} {...el} />
    ));

  return (
    <div
      id={`collapse${id}`}
      className="list collapse"
      aria-labelledby={`heading${id}`}
      data-parent="#accordion"
    >
      <ul className="card-body pt-5">
        {list && list[0] && handleList()}
      </ul>
    </div>
  );
};

export default List;

/*
PropTypes
*/
List.propTypes = {
  id: string.isRequired,
  categoryName: string.isRequired,
};
