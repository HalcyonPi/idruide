const listReducer = (state = [], action) => {
  switch (action.type) {
    case 'GET_LIST':
      return action.list;
    case 'ADD_LIST_ITEM':
      return [...state, action.listItem];
    case 'EDIT_LIST_ITEM':
      return state.filter(el => el._id !== action.listItem._id).concat(action.listItem);
    case 'REMOVE_LIST_ITEM':
      return state.filter(el => el._id !== action.listItem._id);
    default:
      return state;
  }
};

export default listReducer;
