import React from 'react';
import ReactDOM from 'react-dom';
import { register, unregister } from './serviceWorker';

import fontAwesome from './vendors/fontawesome';

/*
STYLING MODULES
*/
import 'normalize.css/normalize.css'; // Reset built-in browser styles
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../styles/styles.scss';

/*
REACT
*/
import AppRouter from './routers/AppRouter';

/*
FONTAWESOME ICONS
*/
fontAwesome();

/*
APP INIT
*/
const jsx = (
  <AppRouter />
);

ReactDOM.render(jsx, document.getElementById('app'));

/*
PWA SERVICE WORKER
*/
register();
