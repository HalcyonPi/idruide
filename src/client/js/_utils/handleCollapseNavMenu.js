const handleCollapseNavMenu = () => {
  const button = document.getElementById('burgerButton');
  const menu = document.getElementById('navBarMainMenu');
  const classCheck = button.classList.contains('collapsed');

  if (!classCheck) {
    button.classList.add('collapsed');
    menu.classList.remove('show');
  }
};

export default handleCollapseNavMenu;
