const handleItemDisplay = (idPrefix, id) => {
  const target = document.getElementById(`${idPrefix}${id}`);

  target.classList.toggle('show-div');
};

export default handleItemDisplay;
