const handleClearState = async (dispatch) => {
  try {
    await dispatch({ type: 'PRODUCTS_RESET' });
  } catch (error) {
    console.log(`Could not reset Redux Store State because: ${error}`);
  }
};

export default handleClearState;
