# idruide test - My Grocies

* User guide:
- From the project's root directory, run in a terminal 'npm run client' to launch the app in a dev mod
- From the project's root directory, run in a terminal 'npm run build:prod' to issue a production ready bundle. Then you may run 'npm start' to launch the app in a production mode.
